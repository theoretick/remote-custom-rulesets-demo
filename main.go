package main

import (
	"fmt"
)

const tmpl = ""

func dangerousEval() {
    // do something dangerous
	return nil
}


func main() {
	fmt.Println("hello world")

	dangerousEval()
}
