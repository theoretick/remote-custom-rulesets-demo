# remote-custom-rulesets-demo

A demonstration project showcasing https://gitlab.com/gitlab-org/gitlab/-/issues/393452

See gitlab.com/theoretick/security-policies-demo-public for an identical public fork of the referenced gitlab.com/theoretick/security-policies-private.

This project uses a remote ruleset configuration as defined by `SAST_RULESET_GIT_REFERENCE` to load a private repositories configuration in place of the default.